<!DOCTYPE html>
<html>
    <body>
    <?php
    // PHP String
    $x = "Hello world!";
    $y = 'Hello world!';

    echo $x;
    echo "<br>";
    echo $y;
    echo "<br>";

    // PHP Integer
    $z = 5985;
    var_dump($z);
    echo "<br>";

    // PHP Float
    $a = 10.365;
    var_dump($a);
    echo "<br>";

    // PHP Array
    $cars = array("Volvo", "BMW", "Toyota");
    var_dump($cars);
    echo "<br>";

    // PHP Object
    class Car {
        public $color;
        public $model;
        public function __construct($color, $model){
            $this->color = $color;
            $this->model = $model;
        }
        public function message() {
            return "My car is a " . $this->color . " " . $this->model . "!";
          }
    }
    $myCar = new Car("black", "Volvo");
    echo $myCar -> message();
    echo "<br>";
    $myCar = new Car("red", "Toyota");
    echo $myCar -> message();


    ?> 
    </body>
</html>