<!DOCTYPE html>
<html>
<body>

<?php
// case-sensitive constant name
define("GREETING", "Welcome to W3Schools.com!");
echo GREETING;
echo "<br>";
echo greeting;
echo "<br>";

// PHP Constant array
define("cars", [
    "Alfa Romeo",
    "BMW",
    "Toyota"
  ]);
  echo cars[0];

  echo "<br>";
// constants area global
function myTest() {
    echo GREETING;
  }
  myTest();
?> 

</body>
</html>