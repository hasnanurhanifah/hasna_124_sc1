<!DOCTYPE html>
<html>
    <body>
        <h1>My First PHP Page</h1>

        <?php
        // PHP keywords not case sensitive
            echo "Hello World! <br>";
            ECHO "Hello World! <br>";
            EcHo "Hello World!<br>";
        
        // PHP case sensitive
            $color = "Red";
            echo "My car is " . $color . "<br>";
            echo "My house is " . $COLOR . "<br>";
            echo "My boat is " . $coLOR . "<br>";


        ?>
    </body>
</html>