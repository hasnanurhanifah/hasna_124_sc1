<!DOCTYPE html>
<html>
<body>
    <?php 
    // Function arguments
    echo "<h3>This is function with one argument</h3>";
    function familyName($fname) {
        echo "$fname Refsnes.<br>";
      }
      
      familyName("Jani");
      familyName("Hege");
      familyName("Stale");
      familyName("Kai Jim");
      familyName("Borge");

      echo "<h3>This is function with two argument</h3>";

      function familyBorn($fname, $year) {
        echo "$fname Refsnes. Born in $year <br>";
      }
      
      familyBorn("Hege","1975");
      familyBorn("Stale","1978");
      familyBorn("Kai Jim","1983");

    // PHP Returning Values
    echo "<h3>This is PHP Returning values</h3>";
    function sum(int $x, int $y) {
        $z = $x + $y;
        return $z;
      }
      
      echo "5 + 10 = " . sum(5,10) . "<br>";
      echo "7 + 13 = " . sum(7,13) . "<br>";
      echo "2 + 4 = " . sum(2,4);
    ?>
</body>
</html>