<!DOCTYPE html>
<html>
<body>

<?php
// pregmatch
echo "<h4>Returns 1 if the pattern was found in the string and 0 if not</h4>";
$str = "Visit W3Schools";
$pattern = "/w3schools/i";
echo preg_match($pattern, $str)."<br>"; 

// pregmatch all
echo "<h4>Returns the number of times the pattern was found in the string, which may also be 0</h4>";
$str = "The rain in SPAIN falls mainly on the plains.";
$pattern = "/ain/i";
echo preg_match_all($pattern, $str)."<br>";

// preg replace
echo "<h4>Returns a new string where matched patterns have been replaced with another string</h4>";
$str = "Visit Microsoft!";
$pattern = "/microsoft/i";
echo preg_replace($pattern, "W3Schools", $str);
?>

</body>
</html>
