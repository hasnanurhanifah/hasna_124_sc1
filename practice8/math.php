<!DOCTYPE html>
<html>
    <body>

    <?php
    // pi function
    echo(pi());
    echo "<br>";

    // min and max function
    echo(min(0, 150, 30, 20, -8, -200) . "<br>");
    echo(max(0, 150, 30, 20, -8, -200));
    echo "<br>";
    // abs function
    echo(abs(-6.7));  // returns 6.7
    echo "<br>";
    // Sqrt function
    echo(sqrt(64)); 
    echo "<br>";

    // round function
    echo(round(0.60));  // returns 1
    echo(round(0.49));

    // random number
    echo(rand());
    ?>

    </body>
</html>