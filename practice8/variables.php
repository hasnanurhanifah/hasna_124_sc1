<!DOCTYPE html>
<html>
    <body>
        <?php 
            // Creating (Declaring) PHP variables
            $txt = "W3Schools.com";
            $x = 5; // global scope
            $y = 10.5;

            // Output Variables
            echo $txt;
            echo "<br>";
            echo $x;
            echo "<br>";
            echo $y;
            echo "<br>";
            echo $x + $y;
            echo "<br>";
            echo "I love $txt!";
            echo "<br>";

            // Variable scope
            function myTest() {
                // using x inside this function will generate an error
                echo "<p>Variable x inside function is: $x </p>";
            }
            myTest();

            echo "<p>Variable x outside function is: $x </p>";
        ?>
    </body>
</html>