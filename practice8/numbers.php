<!DOCTYPE html>
<html>
    <body>
    <?php
    // Check if the type of a variable is integer   
    $x = 5985;
    var_dump(is_int($x));

    echo "<br>";

    // Check again... 
    $x = 59.85;
    var_dump(is_int($x));

    echo "<br>";

    $z = 10.365;
    var_dump(is_float($z));
    
    echo "<br>";

    // Check if a numeric value is finite or infinite 
    $a = 1.9e411;
    var_dump($a);
    echo "<br>";

    // Invalid calculation will return a NaN value
    $b = acos(8);
    var_dump($b);
    echo "<br>";

    // PHP Casting
    // Cast float to int
    $c = 23465.768;
    $int_cast = (int)$c;
    echo $int_cast;

    echo "<br>";
    ?> 
    </body>
</html>