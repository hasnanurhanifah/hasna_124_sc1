<!DOCTYPE html>
<html>
<body>

<?php 

// While Loop
echo "<h2>This is while loop</h2>";
$x = 1;
 
while($x <= 5) {
  echo "The number is: $x <br>";
  $x++;
} 

// PHP do while loop
echo "<h2>This is do while loop</h2>";
$y = 2;
do {
    echo "The number is: $y <br>";
    $y++;
  } while ($y <= 5);


// PHP for loop

echo "<h2>This is for loop</h2>";
for ($i = 0; $i <= 10; $i++) {
    echo "The number is: $i <br>";
  }

// PHP foreach Loop
echo "<h2>This is foreach loop</h2>";
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

foreach($age as $x => $val) {
  echo "$x = $val<br>";
}

?>  

</body>
</html>