<!DOCTYPE html>
<html>
    <body>
        <?php

        // Length of a string
            echo strlen("Hello World!");
            echo "<br>";
        
        // Count words in a string
            echo str_word_count("Hello world!");
            echo "<br>";
        // Reverse a string
        echo strrev("Hello world!");
        echo "<br>";

        // Search for a text within a string
        echo strpos("Hello world!", "world");
        echo "<br>";

        // Replace text within a string
        echo str_replace("world", "Dolly", "Hello world!");
        
        ?>


    </body>
</html>